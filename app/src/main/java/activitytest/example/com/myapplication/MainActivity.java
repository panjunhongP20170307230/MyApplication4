package activitytest.example.com.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    // 成员变量定义，它们是程序界面上的控件/组件，需要在代码中用到
    private EditText editHeight;
    private EditText editWeight;
    private Button btnCalc;
    private TextView textResult;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 隐藏状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        // 初始化控件，findViewById()是根据布局文件中设定的id 值找到组件对象
        editHeight = (EditText) findViewById(R.id.editHeight);
        editWeight = (EditText) findViewById(R.id.editWeight);
        btnCalc = (Button) findViewById(R.id.btnCalc);
        textResult = (TextView) findViewById(R.id.textResult);

        // 响应按钮单击事件
        btnCalc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try {
                    double h = Double.parseDouble(editHeight.getText()
                            .toString()) / 100;
                    double w = Double.parseDouble(editWeight.getText()
                            .toString());

                    double bmi = w / (h * h);

                    if (bmi < 18.5) {
                        textResult.setText(R.string.str_thin);
                    } else if (bmi > 24.9) {
                        textResult.setText(R.string.str_fat);
                    } else {
                        textResult.setText(R.string.str_normal);
                    }
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, R.string.str_error,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.brmi, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // 当单击属性菜单中的某个菜单项时，系统会自动执行onOptionsItemSelected()
        switch (item.getItemId()) {
            case R.id.menu_info:
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, InfoActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.menu_quit:
                finish();
                break;
        }
        // 返回true 代表已处理了单击事件
        return true;
    }

}